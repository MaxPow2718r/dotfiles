" surroundpluggins will be download in that directory"
call plug#begin(stdpath('data') . '/plugged')

" list of pluggins"

Plug 'lervag/vimtex'
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
set conceallevel=1
let g:tex_conceal='abdmg'
" let g:vimtex_latexmk_continouos=1

Plug 'lifepillar/vim-mucomplete'
let g:mucomplete#enable_auto_at_startup=1
" let g:mucomplete#completion_delay = 1

Plug 'ap/vim-css-color'

Plug 'dylanaraps/wal.vim'

Plug 'tpope/vim-surround'

Plug 'tpope/vim-commentary'

Plug 'psf/black', { 'branch': 'stable' }

Plug 'sainnhe/gruvbox-material'

Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

Plug 'SirVer/ultisnips'
let g:UltiSniptsExpandTrigger="<Tab>"
let g:UltiSniptsListSnippets="<c-tab>"
let g:UltiSnipsJumpForwardTrigger="<Space><Space>"
let g:UltiSnipsJumpBackwardTrigger="<Tab><Tab>"
let g:UltiSnipsEditSplit="vertical"
call plug#end()

" gruvbox config

set background=dark
let g:gruvbox_material_background = 'medium'
let g:gruvbox_material_better_performance=1
let g:gruvbox_material_enable_bold=1
let g:gruvbox_material_enable_italic=1
let g:gruvbox_material_transparent_background=1
let g:gruvbox_material_spell_foreground='colored'
colorscheme gruvbox-material

let mapleader = " "

syntax enable
filetype plugin on

" enable spell-check with Ctrl-l"
map <F2> <ESC>:setlocal spell!<CR>
set spelllang=es,en_us
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u

" shows the line number"
set number relativenumber
set cursorline

" Change the cursor shape

let &t_SI = "\<esc>[6 q"
let &t_SR = "\<esc>[6 q"
let &t_EI = "\<esc>[2 q"

" split the window to the right in vertical mode and below in normal mode"
set splitright splitbelow

" enable autocomplete"
set completeopt+=menuone
set completeopt+=noselect
set wildmenu
set wildmode=longest,list,full
set wildignore=*.o,*~
set wildignorecase
set shortmess+=c

" shortcut for windows navigation"
nmap <leader>h <C-w>h
nmap <leader>j <C-w>j
nmap <leader>k <C-w>k
nmap <leader>l <C-w>l

" save and terminal
nmap <Leader>w :w!<CR>
nmap <Leader>t :below vertical terminal<CR>

" hl options
noremap <leader>f :set hlsearch! hlsearch?<CR>

hi CursorLine ctermbg=0 ctermfg=none cterm=none
hi Todo ctermbg=none ctermfg=none cterm=bold,underline
" hi SpellBad ctermbg=none ctermfg=none cterm=bold,underline
" hi IncSearch ctermbg=3 ctermfg=0 cterm=italic
" hi CursorLineFold ctermbg=0* ctermfg=7*
" hi ErrorMsg ctermbg=1 ctermfg=none cterm=bold

" mouse
" if has('clipboard')
set mouse=a
" endif

" search for <++>"
" nnoremap <Space><Space> <ESC>/<++><CR>"_c4l

" Markdown style titles
" =====================
nnoremap <Leader>s yypVr=o<CR>

" complete pair of brackets"
" inoremap () ()<Space><++><ESC>?)<CR>i
" inoremap {} {}<Space><++><ESC>?}<CR>i
" inoremap [] []<Space><++><ESC>?]<CR>i

" git shortcuts
nnoremap <leader>g :!git status<CR>

" compile
map <F5> :make<CR>

" Explore netrw options
nnoremap <leader><Tab> :Lexplore<CR>
let g:netrw_winsize=30
let g:netrw_liststyle=1
let g:netrw_banner=0
let g:netrw_hide=1
let g:netrw_list_hide= netrw_gitignore#Hide()
let g:netrw_localcopydircmd='cp -r'
let g:netrw_special_syntax=1
let g:netrw_sizestyle='H'

" open man pages on vim now <leader>K will open a man page on a split in a
" vim-buffer
runtime! ftplugin/man.vim

" Show a few lines of context around the cursor.  Note that this makes the
" text scroll if you mouse-click near the start or end of the window.
" set scrolloff=5

" Don't use Q for Ex mode, use it for formatting.  Except for Select mode.
" Revert with ":unmap Q".
map Q gq
sunmap Q

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
" Revert with ":iunmap <C-U>".
inoremap <C-U> <C-G>u<C-U>

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
" Revert with: ":delcommand DiffOrig".
if !exists(":DiffOrig")
	command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
				\ | wincmd p | diffthis
endif

augroup vimStartup
	au!

	" When editing a file, always jump to the last known cursor position.
	" Don't do it when the position is invalid, when inside an event handler
	" (happens when dropping a file on gvim) and for a commit message (it's
	" likely a different one than last time).
	autocmd BufReadPost *
				\ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
				\ |   exe "normal! g`\""
				\ | endif


	autocmd FileType text setlocal textwidth=78

	" reload sxhkd
	autocmd BufWritePost ~/dotfiles/configs/.config/sxhkd/sxhkdrc !killall sxhkd;sxhkd &

	" reload picom
	autocmd BufWritePost ~/dotfiles/configs/.config/picom/picom.conf silent !killall picom;picom -b

	" automatically deletes all whitespace on save"
	autocmd BufWritePre * %s/\s\+$//e

	" autocompile slstatus
	autocmd BufWritePost /hdd/Downloads/Repos/slstatus/config.def.h
				\ !rm -f config.h; sudo make clean install;
				\ killall slstatus;setsid slstatus&

	" latex commands"
	autocmd VimLeave *.tex !latexmk -c %

	" snippets commentary
	autocmd Filetype snippets setlocal commentstring=#\ %s

	" splits vertical by default
	" autocmd WinNew * wincmd L
augroup END


" toogle quickfix buffer
function! GetBufferList()
	redir =>buflist
	silent! ls!
	redir END
	return buflist
endfunction

function! ToggleList(bufname, pfx)
	let buflist = GetBufferList()
	for bufnum in map(filter(split(buflist, '\n'), 'v:val =~ "'.a:bufname.'"'), 'str2nr(matchstr(v:val, "\\d\\+"))')
		if bufwinnr(bufnum) != -1
			exec(a:pfx.'close')
			return
		endif
	endfor
	if a:pfx == 'l' && len(getloclist(0)) == 0
		echohl ErrorMsg
		echo "Location List is Empty."
		return
	endif
	let winnr = winnr()
	exec(a:pfx.'open')
	if winnr() != winnr
		wincmd p
	endif
endfunction

nmap <silent> <leader>e :call ToggleList("Quickfix List", 'c')<CR>

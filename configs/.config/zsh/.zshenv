# set PATH so it includes user's private bin if it exists
export PATH="$HOME/.local/scripts:$PATH"
export PATH="/home/maxp/.local/scripts/status_blocks/:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.local/share/python/bin:$PATH"
export PATH="/home/maxp/.local/share/torbrowser/:$PATH"

export OPENAI_API_KEY=$(cat $HOME/.config/openai.token)

export EDITOR="vim"
export PDFVIEWER="zathura"
export BROWSER="surf"
export VIEWER="mpv"
export IMAGE="sxiv"
export WALLDIR="/hdd/Media/wall"

# clean home

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"

export PYTHONPYCACHEPREFIX=$XDG_CACHE_HOME/python
export PYTHONUSERBASE=$XDG_DATA_HOME/python
export DOCKER_CONFIG="$XDG_CONFIG_HOME"/docker
export WINEPREFIX="$XDG_DATA_HOME"/wineprefixes/default
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export TEXMFHOME="$XDG_DATA_HOME"/texmf
export TEXMFVAR="$XDG_CACHE_HOME"/texlive/texmf-var
export TEXMFCONFIG="$XDG_CONFIG_HOME"/texlive/texmf-config
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export GOPATH="$XDG_DATA_HOME"/go
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
export QT_QPA_PLATFORMTHEME=qt5ct
#compinit -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION

# others
export MANGOHUD=1
. "/home/maxp/.local/share/cargo/env"

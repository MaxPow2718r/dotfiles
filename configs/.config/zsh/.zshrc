# Set up the prompt

# Enable colors and change prompt:
autoload -Uz colors  && colors
autoload -Uz promptinit
promptinit
PROMPT='%B%F{$color3}[%n%f%F{$color7}%b@%B%f%F{$color4}%m%f %F{$color5}%# %c]%B%b%f '
RPROMPT='${vcs_info_msg_0_}'

setopt histignorealldups sharehistory
# Keep 10000 lines of history within the shell
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/.zsh_history
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_find_no_dups

# Use vi keys
bindkey -v
export KEYTIMEOUT=1

# Use modern completion system
autoload -Uz compinit
compinit

# Git on prompt
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git svn
precmd() {
		vcs_info
}
setopt prompt_subst
zstyle ':vcs_info:git*' formats "[%b]"

# Autocomplete
setopt autocd
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

# History search
typeset -g -A key

key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"

autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

bindkey '^p' history-search-backward
bindkey '^n' history-search-forward

[[ -n "${key[Up]}"   ]] && bindkey -- "${key[Up]}"   up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search

if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

# Load wal colors
. "$HOME/.local/share/colors.sh"

# Aliases
alias ls='ls --color=auto'
alias la='ls -A --color=auto'
alias ll='ls -lhA --color=auto --group-directories-first'
alias rm='rm -vI'
alias cp='cp -vi'
alias mv='mv -vi'
alias grep='grep --color=auto'
alias wget='wget --hsts-file="$XDG_CACHE_HOME/wget-hsts"'
alias sgame='xinit /usr/bin/steam -- :1 vt$XDG_VTNR'
alias maicra='prime-run minecraft-launcher'
alias s='source env/bin/activate'
alias d='deactivate'
alias gis='git status'
alias gc='git commit'
alias ga='git add'
alias gl='git log -P'
alias gd='git diff HEAD'
alias switch='ddcutil setvcp 60 0x11'
alias arduino-cli='arduino-cli --config-file $XDG_CONFIG_HOME/arduino15/arduino-cli.yaml'
alias nvidia-settings='nvidia-settings --config=$XDG_CONFIG_HOME/nvidia/settings'

# autostart vim

v() {
	if [ -n "$1" ]
	then
		vim --servername vim "$1"
	else
		vim --servername vim .
	fi
}


# Autostart
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec startx
fi

export FZF_DEFAULT_OPTS='--height 40% --layout=reverse'
export YT_API_PASS=$(< $HOME/.local/yt-credential.sh)

# make a new directory and cd into it
cdir() {
	command mkdir $1 && cd $1
}

# Change cursor shape
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Use fzf to open

# Files
fa() {
	local dir
	IFS=$'\n' files=($(fzf-tmux --query="$1" --multi --select-1 --exit-0))
	#dir=$(dirname "$files") && cd "$dir"
	[[ -n "$files" ]] && ${EDITOR:-vim} "${files[@]}"
}

# Repos
rep() {
	local dir
	dir=$(find '/hdd/Downloads/Repos/' -maxdepth 1 \
		-type d -print 2> /dev/null | fzf +m) &&
		cd "$dir"
}

# Directory
fd() {
	local dir
	dir=$(find -L ${1:-.} -path '*/\.*' -prune \
		-o -type d -print 2> /dev/null | fzf +m) &&
		cd "$dir"
}

# Allow CTRL-v to open the current command on a vim buffer
autoload -z edit-command-line
zle -N edit-command-line
bindkey -M vicmd ^v edit-command-line

autoload -Uz bashcompinit && bashcompinit
source /hdd/Downloads/Repos/qmk_firmware/util/qmk_tab_complete.sh
source /home/maxp/.local/share/cargo/env

[ -f ~/.local/bin/lazyshell.zsh ] && source ~/.local/bin/lazyshell.zsh
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

" Vim filetype plugin file
" Language: c

setlocal textwidth=80
nmap <F5> :make run<CR>

autocmd BufWritePre *.c
    \  if !filereadable(expand('%'))
    \|   let b:is_new = 1
    \| endif
autocmd BufWritePost *.c
    \  if get(b:, 'is_new', 0)
    \|   silent execute '! new_mkfile %'
    \|   let b:is_new = 0
    \| endif

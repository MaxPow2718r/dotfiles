" Vim filetype plugin file
" Language: markdown

setlocal textwidth=80
setlocal spelllang=es,en_us

map <F5> :silent !litemdview %&<CR>

let g:mucomplete#enable_auto_at_startup = 0

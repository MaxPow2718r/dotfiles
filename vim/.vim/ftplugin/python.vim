" Vim filetype plugin file
" Language: python

augroup black_on_save
  autocmd!
  autocmd BufWritePre *.py Black
augroup end

set noautochdir
compiler python

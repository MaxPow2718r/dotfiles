" Vim filetype plugin file
" Language: rust

" Autoformat code
if executable('rustfmt')
	setlocal formatprg=rustfmt\ -q
	setlocal formatexpr=
endif

set noautochdir
setlocal textwidth=80
nmap <F5> :make run<CR>
compiler cargo

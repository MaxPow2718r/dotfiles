" Vim filetype plugin file
" Language: LaTeX

setlocal textwidth=80
set spelllang=es,en_us
set spell

let g:mucomplete#force_manual=1
